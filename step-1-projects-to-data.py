#!/usr/bin/env python

import csv
from eth_account import account
from json import dumps

# It is assumed that the script is run in an environment that has the eth_account
# module installed.
# It can be done for example in the anaconda docker:
# $ sudo docker pull continuumio/anaconda3
# $ sudo docker run -i -t --rm -p 8888:8888 continuumio/anaconda3 \
#     /bin/bash -c "/opt/conda/bin/conda install jupyter -y --quiet; \
#     pip install eth_account; \
#     mkdir /opt/notebooks; \
#     /opt/conda/bin/jupyter notebook --notebook-dir=/opt/notebooks --ip='0.0.0.0' --port=8888 --no-browser --allow-root"


# The script expects the csv file that keeps informaton about repos in form of
# <group-of-participants>;<group-of-projects>

acc_obj = account.Account()

new_file_data = {}

with open('repos.csv') as csvfile:
    d = csv.reader(csvfile, delimiter=";")
    for row in d:
        print(row)
        pk = account.keccak(row[1].encode())
        acc = acc_obj.privateKeyToAccount(pk)
        print(f'{acc.address}: {acc.privateKey.hex()[2:]}')
        new_file_data[row[1]] = {}
        new_file_data[row[1]]['group'] = row[0]
        new_file_data[row[1]]['addr'] = acc.address
        new_file_data[row[1]]['pk'] = acc.privateKey.hex()[2:]

to_write = dumps(new_file_data)
with open('repos.json', 'w') as jsonfile:
    jsonfile.write(to_write)
