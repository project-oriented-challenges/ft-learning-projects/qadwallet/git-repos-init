#!/usr/bin/env python

from json import load

# This script assumes that the JSON file with the project data exists
# in the current directory.
# The script will produce the shell file containing the steps to
# intialized all projects listed in the JSON data.

with open('repos.json') as jsonfile:
    repos = load(jsonfile)

META_GROUP="onti-fintech/onti-2020-fintech/"
#META_GROUP=""

exec_file = "#!/bin/bash\n\n"

for i in repos.keys():
    gr   = repos[i]['group']
    addr = repos[i]['addr']
    pk   = repos[i]['pk']
    exec_file+=f'echo "generate repo for {i}"\n'
    exec_file+="cp -a template repo\n"
    exec_file+=f'sed -i .bak "s/REPLACE/{pk}/" repo/.gitlab-ci.yml\n'
    exec_file+=f'sed -i .bak "s/REPLACE/{addr}/" repo/README.md\n'
    exec_file+=f'sed -i .bak "s/REPLACE/{addr}/" repo/README-ru.md\n'
    exec_file+="cd repo\n"
    exec_file+="rm .gitlab-ci.yml.bak README.md.bak README-ru.md.bak\n"
    exec_file+="git init\n"
    #exec_file+=f'git remote add origin git@gitlab.com:alex.kolotov/test-and-remove.git\n'
    if len(META_GROUP) != 0:
        exec_file+=f'git remote add origin git@gitlab.com:{META_GROUP}{gr}/{i}/qadwallet.git\n'
    else:
        exec_file+=f'git remote add origin git@gitlab.com:{gr}/{i}/qadwallet.git\n'
    exec_file+="git add .\n"
    exec_file+="git commit -m 'Initial commit from template [skip ci]'\n"
    exec_file+="git push -u origin master\n"
    exec_file+="cd ..\n"
    exec_file+="rm -rf repo\n\n"
    
with open('init-repos.sh', 'w') as script_file:
    script_file.write(exec_file)
