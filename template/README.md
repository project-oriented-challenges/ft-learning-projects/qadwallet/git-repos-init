Assignement "Quick and Dirty wallet"
====

## Introduction

This repository is dedicated to keep the solution of the assignment "Quick and dirty wallet".

The task description can be found [here](task-description.md).

## Pre-requisites 

First of all, you need to create a branch `develop` in this repository. Then clone the repository to you local machine and prepare the solution for the given task.

In fact, you need to develop a python script which **MUST** be called `qadwallet.py`.

Before starting the writing the code, you need to get familiar with the `web3`  python library. It is recommended to read its excellent [documentation](http://web3py.readthedocs.io).

Install the library in your python environment. E.g. it could be done by executing:

```shell
$ pip install web3
```

By using this library your script will communicate with a blockchain node based on Ethereum Virtual Machine. JSON RPC channel will be used for the communication. The description of the corresponding API is available [here](https://wiki.parity.io/JSONRPC-eth-module).

Pay attention on the following calls:
  * [eth.blockNumber](https://wiki.parity.io/JSONRPC-eth-module#eth_blocknumber)
  * [eth.getBlockByNumber](https://wiki.parity.io/JSONRPC-eth-module#eth_getblockbynumber)
  * [eth.getTransactionByHash](https://wiki.parity.io/JSONRPC-eth-module#eth_gettransactionbyhash)
  * [eth.getBalance](https://wiki.parity.io/JSONRPC-eth-module#eth_getbalance)
  * [eth.sendRawTransaction](https://wiki.parity.io/JSONRPC-eth-module#eth_sendrawtransaction)
  * [eth.getTransactionReceipt](https://wiki.parity.io/JSONRPC-eth-module#eth_gettransactionreceipt)
  * [eth.getTransactionCount](https://wiki.parity.io/JSONRPC-eth-module#eth_gettransactioncount)

All of them are wrapped by the `web3` library.

Your solution must work with the testnet Sokol. The JSON RPC node working with the Sokol chain can be accessed by [https://sokol.poa.network](https://sokol.poa.network).  

In order to get some amount of coins to test your solution, the [_Sokol faucet_](https://faucet-sokol.herokuapp.com/) should be used. Specify an account address which will be founded with `0.5` test _poa_ coins. Note that several requests can be performed to get more coins!

It is possible to monitor state of blocks and transactions within the Sokol chain. Go to the block explorer [Blockscout](https://blockscout.com/poa/sokol/) for this. In the top right corner you can enter an account address, a transaction hash, a number or hash of a block. Try to access it now and perform few experiments.

Your solution must work with private keys that corresponds to accounts in the Sokol network. The information how to start developing Ethereum-applications that use private keys, please read the section ["Working with Local Private Keys"
](https://web3py.readthedocs.io/en/stable/web3.eth.account.html#) of `web3` documentation.

One of quickest way to get a private key and the address corresponding to the key is to use [MyEtherWallet.com](http://myetherwallet.com). Create a wallet on this site and after uploading the keystore file (file with the private key encrypted) the browser will display the private key in the hexadecimal form.

### Solution verification

Take into account the requirements presented in the examples of [the task description](task-description.md). Your solution will be tested whether it satisfies these requirements.

Every time you `push` the changes to the `develop` branch the test of your solution will be started automatically. Result of tests will be available on GitLab server in the section `CI/CI -> Pipelines`.

If the tests were passed, you will see the corresponding green label on top of the list with pipelines

If some tests are failed, the label will be red. Click on the label to see execution of exact test. You will see the log how your script was run with different parameters. For example:

```
qadwallet.py --tx 0xf026...1bbc
--- ACTUAL RESPONSE:
No such transaction in the local copy of the chain
--- EXPECTED RESPONSE:
No such transaction in the chain
FAILED
TEST 010: Sending transaction when no enough funds.
EXECUTED:
qadwallet.py --key 154A7...536E --to 8b78...23d9A --value 500000000000000000000000
PASSED

Total tests run: 11
Failed tests: 1
ERROR: Job failed: exit code 1
```

Just after the creation of the branch `develop` the tests will be executed and definitely they will be failed. It is OK. Firstly, you did not push the solution on this stage yet. Secondly, the account which is being used for tests has no enough funds to make transactions. That's why **before the very first `push` please fund your account REPLACE with `2.5` _Sokol poa_ on [_faucet_](https://faucet-sokol.herokuapp.com/)**.

Don't change `.gitlab-ci.yml` since it is used to run configured automated testing in GitLab..

### Solution acceptance

As soon as you think that your solution is ready (completely or not), please create a _Merge Request_ in GitLab to merge changes from the `develop` branch to the `master` branch. Assign the Merge Request to the person who is responsible to grade it.
