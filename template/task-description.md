## Task description

A basic operation which could be performed on every (almost) Ethereum node is to get the balance of the account or transfer some amount of _ether_ to another account. This minimal functionality is being performed by that part of a node software which is called a cryptocurrency wallet.

Your task is automate the functions described above.

You need to develop a command line application (a python script) with support of the following operations:
  * output an account balance
  * send some amount of _POA_ coins to another account

The script must be named `qadwallet.py` and must perform actions by operating with a private key of an Ethereum account. The private key is specified in the hexadecimal form.

### Usage examples

#### Output the balance

This is the first example (_US-01_) of the script usage:
```shell
$ qadwallet.py --key c5d2460186f7233c927e7db2dcc703c0e500b653ca82273b7bfad8045d85a470
```
The standard output must be like the following:
```
Balance on "9cce34F7aB185c7ABA1b7C8140d620B4BDA941d6" is 2.5 poa
```

#### Send POA coins

In order to send some amount of _poa_ coins owning by an account associated with a private key, it is necessary to point out the recipient and amount of cons. The recipient address must be specified in the hexadecimal form. The amount to send is an integer number -- amount of coins in _Wei_.

The script must work in both cases:
  * when this is the very first transaction signed by the specified private key
  * when the blockchain already contains the transactions signed by the specified private key

The second example (_US-02_) of the script usage:
```shell
$ qadwallet.py --key c5d2460186f7233c927e7db2dcc703c0e500b653ca82273b7bfad8045d85a470 --to EAee08c91B13d8d7A82850b714C7c68826E9CFAe --value 500000000000000000
```
The standard output must be like the following:
```
Payment of 500 finney to "EAee08c91B13d8d7A82850b714C7c68826E9CFAe" scheduled
Transaction Hash: 0x27c9181caeb55d37e1105fa1a8648db7fe50f79064b98e56b8e854e3abb43728
```

Another example (_US-03_) demonstrating automatic scale of the coins amount in the output:
```shell
$ qadwallet.py --key c5d2460186f7233c927e7db2dcc703c0e500b653ca82273b7bfad8045d85a470 --to EAee08c91B13d8d7A82850b714C7c68826E9CFAe --value 2500000000000000000
Payment of 2.5 poa to "EAee08c91B13d8d7A82850b714C7c68826E9CFAe" scheduled
Transaction Hash: 0x27d6236b24f3643fb6f55442572f0a382bc6fc3674ef64a62f706591235f0ecf
```

Amount scale is the result of the normalization to one of the possibles values: _poa_, _finney_, _szabo_, _gwei_, _mwei_, _kwei_, _wei_. The rule for the normalization:
  * The maximum possible normalized value where the the integer part (before the dot) is more than zero;
  * The fractional portion is accounted with rounding values up to $10^{-6}$;
  * Leading zeros in the fractional portion are not outputted (e.g. `1.3` rather than `1.300000`)

The script must inform the user if the account balance is not sufficient to send coins (_US-04_):
```shell
$ qadwallet.py --key c5d2460186f7233c927e7db2dcc703c0e500b653ca82273b7bfad8045d85a470 --to EAee08c91B13d8d7A82850b714C7c68826E9CFAe --value 250000000000000000000
No enough funds for payment
```

#### Transaction status

The fourth example (_US-05_) demonstrating the ability to get the status of a transaction used to send coins:
```shell
$ qadwallet.py --tx 0x27c9181caeb55d37e1105fa1a8648db7fe50f79064b98e56b8e854e3abb43728
```
The standard output must be like the following:
```
Payment of 500 finney to "EAee08c91B13d8d7A82850b714C7c68826E9CFAe" confirmed
```

Or if the transaction is still not confirmed (_US-06_):
```shell
$ qadwallet.py --tx 0x27d6236b24f3643fb6f55442572f0a382bc6fc3674ef64a62f706591235f0ecf
Delay in payment of 2.5 poa to "EAee08c91B13d8d7A82850b714C7c68826E9CFAe"
```

Attempt to get information about inexistent transaction must be properly handled (_US-07_):
```shell
$ qadwallet.py --tx 0x27d6236b24f3643fb6f55442572f0a382bc6fc3674ef64a62f706591235ffce0
No such transaction in the chain
```

### Note

The script must work with a remote node only through RPC. For this assignment the RPC URL must be hardcoded and must be `https://sokol.poa.network`.
